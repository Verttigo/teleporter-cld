package ch.verttigo.craftok.teleport;


import ch.verttigo.craftok.teleport.Commands.TeleportCommand;
import ch.verttigo.craftok.teleport.Commands.TeleportHereCommand;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import org.bukkit.plugin.java.JavaPlugin;

public class Teleport extends JavaPlugin {

    public static Teleport instance;

    public static Teleport getInstance() {
        return Teleport.instance;
    }

    public void onEnable() {
        this.getCommand("ctp").setExecutor(new TeleportCommand());
        this.getCommand("ctphere").setExecutor(new TeleportHereCommand());
        Teleport.instance = this;
        CloudNetDriver.getInstance().getEventManager().registerListener(new PluginMessageListener());
    }
}
