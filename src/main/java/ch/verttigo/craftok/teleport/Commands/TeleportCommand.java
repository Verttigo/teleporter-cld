package ch.verttigo.craftok.teleport.Commands;

import ch.verttigo.craftok.teleport.Teleport;
import ch.verttigo.craftok.teleport.Utils.Utils;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import de.dytanic.cloudnet.ext.bridge.BridgeServiceProperty;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportCommand implements CommandExecutor {
    public static final IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);

    public boolean onCommand(final CommandSender commandSender, final Command cmd, final String msg, final String[] args) {
        if (commandSender.hasPermission("craftoktp.tp")) {
            Player p = (Player) commandSender;
            if (args.length == 0) {
                commandSender.sendMessage(Utils.returnMessage("noPlayerEntered"));
                return true;
            }
            if (Utils.getPlayer(args[0]) == null) {
                commandSender.sendMessage(Utils.returnMessage("unknownPlayer"));
                return true;
            }

            ICloudPlayer senderPCloud = playerManager.getOnlinePlayer(p.getUniqueId());
            ICloudPlayer targetPCloud = Utils.getPlayer(args[0]);
            ServiceInfoSnapshot senderServer = CloudNetDriver.getInstance().getCloudServiceProvider().
                    getCloudService(senderPCloud.getConnectedService().getUniqueId());
            ServiceInfoSnapshot targetServer = CloudNetDriver.getInstance().getCloudServiceProvider().
                    getCloudService(targetPCloud.getConnectedService().getUniqueId());

            if (args.length == 1 && args[0] != null) {
                if (senderPCloud.getConnectedService().getServerName().equalsIgnoreCase(targetPCloud.getConnectedService().getServerName())) {
                    final Player target = Bukkit.getPlayer(args[0]);
                    p.teleport(target);
                    commandSender.sendMessage(Utils.returnMessage("teleportSameServer", senderPCloud, targetPCloud, senderServer, targetServer));
                } else {
                    if (senderServer.getProperty(BridgeServiceProperty.MOTD).get().equalsIgnoreCase("ingame")) {
                        final String message = Utils.returnMessage("playerInGame", senderPCloud, targetPCloud, senderServer, targetServer);
                        final TextComponent click = new TextComponent(Teleport.getInstance().getConfig().getString("confirm"));
                        click.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/stp " + targetPCloud.getName() + " confirm"));
                        p.sendMessage(message);
                        p.spigot().sendMessage(click);
                        return true;
                    } else {
                        Utils.teleportToPlayer(senderPCloud, targetPCloud, targetServer);
                        senderPCloud.getPlayerExecutor().sendChatMessage(Utils.returnMessage("teleportToPlayer", senderPCloud, targetPCloud, senderServer, targetServer));
                    }
                }
            }
            if (args.length > 1 && args[0] != null && args[1].equalsIgnoreCase("confirm")) {
                Utils.teleportToPlayer(senderPCloud, targetPCloud, targetServer);
                senderPCloud.getPlayerExecutor().sendChatMessage(Utils.returnMessage("teleportToPlayer", senderPCloud, targetPCloud, senderServer, targetServer));
            }
        }
        return true;
    }

}
