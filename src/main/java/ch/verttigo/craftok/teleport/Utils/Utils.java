package ch.verttigo.craftok.teleport.Utils;

import ch.verttigo.craftok.teleport.Teleport;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import org.bukkit.Bukkit;

public class Utils {

    public static final IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);

    public static ICloudPlayer getPlayer(String s) {
        for (ICloudPlayer p : playerManager.getOnlinePlayers()) {
            if (s.equalsIgnoreCase(p.getName())) return p;
        }
        return null;
    }

    public static void teleportToPlayer(ICloudPlayer senderPCloud, ICloudPlayer targetPCloud, ServiceInfoSnapshot targetServer) {
        senderPCloud.getPlayerExecutor().connect(targetServer.getName());
        Bukkit.getScheduler().scheduleSyncDelayedTask(Teleport.getInstance(), () -> CloudNetDriver.getInstance().getMessenger().sendChannelMessage(targetServer, "TELEPORT", "TELEPORT", new JsonDocument()
                .append("SenderName", senderPCloud.getName())
                .append("TargetName", targetPCloud.getName())
        ), 20L);
    }

    public static void teleportPlayerHere(ICloudPlayer senderPCloud, ICloudPlayer targetPCloud, ServiceInfoSnapshot senderServer) {
        targetPCloud.getPlayerExecutor().connect(senderServer.getName());
        Bukkit.getScheduler().scheduleSyncDelayedTask(Teleport.getInstance(), () -> CloudNetDriver.getInstance().getMessenger().sendChannelMessage(senderServer, "TELEPORT", "TELEPORT", new JsonDocument()
                .append("SenderName", targetPCloud.getName())
                .append("TargetName", senderPCloud.getName())
        ), 20L);
    }

    public static String returnMessage(String messageFromConfig, ICloudPlayer senderPCloud, ICloudPlayer targetPCloud, ServiceInfoSnapshot senderServer, ServiceInfoSnapshot targetServer) {
        return Teleport.getInstance().getConfig().getString(messageFromConfig).
                replace("%SenderPlayerName%", senderPCloud.getName()).
                replace("%TargetPlayerName%", targetPCloud.getName()).
                replace("%TargetServerName%", targetServer.getName()).
                replace("%SenderServerName%", senderServer.getName());
    }

    public static String returnMessage(String messageFromConfig) {
        return Teleport.getInstance().getConfig().getString(messageFromConfig);
    }
}
