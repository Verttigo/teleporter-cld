package ch.verttigo.craftok.teleport;


import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.events.channel.ChannelMessageReceiveEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class PluginMessageListener  {

    @EventListener
    public void handleChannelMessage(ChannelMessageReceiveEvent event) {
        if (event.getChannel().equalsIgnoreCase("TELEPORT")) {
            Bukkit.getScheduler().runTask(Teleport.getInstance(), () -> {
                Player sender = Bukkit.getPlayer(event.getData().getString("SenderName"));
                Player target = Bukkit.getPlayer(event.getData().getString("TargetName"));
                sender.teleport(target);
            });
        }
    }
}
